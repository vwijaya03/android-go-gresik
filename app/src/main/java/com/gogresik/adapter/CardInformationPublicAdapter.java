package com.gogresik.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.gogresik.R;
import com.gogresik.activity.DetailBerita;
import com.gogresik.helper.Helper;
import com.gogresik.model.BeritaItem;
import com.gogresik.model.InformationPublicItem;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CardInformationPublicAdapter extends RecyclerView.Adapter<CardInformationPublicAdapter.ViewHolder> {

    List<InformationPublicItem> items;

    public CardInformationPublicAdapter(List<InformationPublicItem> items) {
        super();
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_information_public, parent, false);

        final ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        InformationPublicItem list = items.get(position);

        holder.tvTitleInformationPublic.setText(list.getTitle());
        holder.tvDescriptionInformationPublic.setText(Helper.stripHtml(list.getDescription()));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvTitleInformationPublic, tvDescriptionInformationPublic;

        public ViewHolder(View itemView) {
            super(itemView);

            tvTitleInformationPublic = itemView.findViewById(R.id.title_information_public);
            tvDescriptionInformationPublic = itemView.findViewById(R.id.description_information_public);
        }
    }
}