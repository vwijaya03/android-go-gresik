package com.gogresik.adapter;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.provider.CalendarContract;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.gogresik.R;
import com.gogresik.activity.DetailBerita;
import com.gogresik.activity.DetailEvent;
import com.gogresik.helper.Helper;
import com.gogresik.model.EventItem;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class CardEventAdapter extends RecyclerView.Adapter<CardEventAdapter.ViewHolder> {

    List<EventItem> items;

    public CardEventAdapter(List<EventItem> items) {
        super();
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_event, parent, false);

        final ViewHolder viewHolder = new ViewHolder(v);

        viewHolder.ivImgEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), DetailEvent.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.putExtra("img", items.get(viewHolder.getAdapterPosition()).getImg_url());
                i.putExtra("title", items.get(viewHolder.getAdapterPosition()).getTitle());
                i.putExtra("date_event", items.get(viewHolder.getAdapterPosition()).getEventDate());
                i.putExtra("created_at", items.get(viewHolder.getAdapterPosition()).getCreatedAt());
                i.putExtra("description", items.get(viewHolder.getAdapterPosition()).getDescription());

                v.getContext().startActivity(i);
            }
        });

        viewHolder.btnDetailEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), DetailEvent.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.putExtra("img", items.get(viewHolder.getAdapterPosition()).getImg_url());
                i.putExtra("title", items.get(viewHolder.getAdapterPosition()).getTitle());
                i.putExtra("date_event", items.get(viewHolder.getAdapterPosition()).getEventDate());
                i.putExtra("created_at", items.get(viewHolder.getAdapterPosition()).getCreatedAt());
                i.putExtra("description", items.get(viewHolder.getAdapterPosition()).getDescription());

                v.getContext().startActivity(i);
            }
        });

        viewHolder.btnBookmarEvent.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_INSERT);
                i.setType("vnd.android.cursor.item/event");
                i.putExtra(CalendarContract.Events.TITLE, items.get(viewHolder.getAdapterPosition()).getTitle());
                i.putExtra(CalendarContract.Events.EVENT_LOCATION, "Gresik");
                i.putExtra(CalendarContract.Events.DESCRIPTION, items.get(viewHolder.getAdapterPosition()).getTitle());
                i.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, Helper.convertToMilisecond(items.get(viewHolder.getAdapterPosition()).getEventDate()));
                v.getContext().startActivity(i);
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        EventItem list = items.get(position);

        Picasso.get().load(list.getImg_url()).into(holder.ivImgEvent);
        holder.tvTitleEvent.setText(list.getTitle());
        holder.tvDateEvent.setText("Tanggal Event: "+list.getEventDate());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivImgEvent;
        public TextView tvTitleEvent, tvDateEvent;
        public Button btnDetailEvent, btnBookmarEvent;

        public ViewHolder(View itemView) {
            super(itemView);

            ivImgEvent = itemView.findViewById(R.id.img_event);
            tvTitleEvent = itemView.findViewById(R.id.title_event);
            tvDateEvent = itemView.findViewById(R.id.date_event);
            btnDetailEvent = itemView.findViewById(R.id.detail_event);
            btnBookmarEvent = itemView.findViewById(R.id.bookmark_event);
        }
    }
}