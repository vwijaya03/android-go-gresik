package com.gogresik.adapter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.provider.CalendarContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.gogresik.R;
import com.gogresik.activity.DetailEvent;
import com.gogresik.activity.DetailWisata;
import com.gogresik.helper.Helper;
import com.gogresik.helper.PermissionUtils;
import com.gogresik.model.EventItem;
import com.gogresik.model.WisataItem;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CardWisataAdapter extends RecyclerView.Adapter<CardWisataAdapter.ViewHolder> {

    List<WisataItem> items;

    private static final int REQUEST_PERMISSIONS_LOCATION_SETTINGS_REQUEST_CODE = 33;
    private static final int REQUEST_PERMISSIONS_LAST_LOCATION_REQUEST_CODE = 34;
    private int REQUEST_PERMISSIONS_CURRENT_LOCATION_REQUEST_CODE = 35;

    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback mLocationCallback;

    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;

    double latitude;
    double longitude;
    boolean isPermissionGranted;
    private String fromLatLng = "";

    // list of permissions
    ArrayList<String> permissions = new ArrayList<>();
    PermissionUtils permissionUtils;

    private LocationRequest mLocationRequest;
    private static long MIN_UPDATE_INTERVAL = 60 * 1000; // 1  minute is the minimum Android recommends, but we use 30 seconds

    public CardWisataAdapter(List<WisataItem> items) {
        super();
        this.items = items;
    }

    @SuppressLint("MissingPermission")
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_wisata, parent, false);

        final ViewHolder viewHolder = new ViewHolder(v);

        checkForLocationRequest();

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(v.getContext());

        mFusedLocationClient.requestLocationUpdates(mLocationRequest, new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {

                if(locationResult != null) {
                    fromLatLng = String.valueOf(locationResult.getLastLocation().getLatitude()+","+locationResult.getLastLocation().getLongitude());
                }
            }
        }, Looper.myLooper());

        viewHolder.ivImgWisata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), DetailWisata.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.putExtra("id", items.get(viewHolder.getAdapterPosition()).getId());
                i.putExtra("img", items.get(viewHolder.getAdapterPosition()).getImg_url());
                i.putExtra("title", items.get(viewHolder.getAdapterPosition()).getTitle());
                i.putExtra("kategori", items.get(viewHolder.getAdapterPosition()).getKategori());
                i.putExtra("created_at", items.get(viewHolder.getAdapterPosition()).getCreatedAt());
                i.putExtra("description", items.get(viewHolder.getAdapterPosition()).getDescription());

                v.getContext().startActivity(i);
            }
        });

        viewHolder.btnDetailWisata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), DetailWisata.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.putExtra("id", items.get(viewHolder.getAdapterPosition()).getId());
                i.putExtra("img", items.get(viewHolder.getAdapterPosition()).getImg_url());
                i.putExtra("title", items.get(viewHolder.getAdapterPosition()).getTitle());
                i.putExtra("kategori", items.get(viewHolder.getAdapterPosition()).getKategori());
                i.putExtra("created_at", items.get(viewHolder.getAdapterPosition()).getCreatedAt());
                i.putExtra("description", items.get(viewHolder.getAdapterPosition()).getDescription());

                v.getContext().startActivity(i);
            }
        });

        viewHolder.btnGetDirectionWisata.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(final View v) {

                if(fromLatLng != null) {
                    String toLatLng = String.valueOf(items.get(viewHolder.getAdapterPosition()).getLat()+","+items.get(viewHolder.getAdapterPosition()).getLng());

                    Intent googleMapIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://maps.google.com/maps?saddr="+fromLatLng+"&daddr="+toLatLng));
                    v.getContext().startActivity(googleMapIntent);
                }
            }
        });

        return viewHolder;
    }

    public void checkForLocationRequest(){
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setInterval(MIN_UPDATE_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }

    private void startLocationPermissionRequest(int requestCode, Context activity) {
        ActivityCompat.requestPermissions((Activity) activity, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, requestCode);
    }

    private void requestPermissions(final int requestCode, final Context activity) {
        boolean shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale((Activity) activity, Manifest.permission.ACCESS_COARSE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            AlertDialog.Builder ad = new AlertDialog.Builder(activity);
            ad.setTitle("Permission Required");
            ad.setMessage("");
            ad.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startLocationPermissionRequest(requestCode, activity);
                }
            });
            ad.setCancelable(false);
            ad.show();

        } else {
            startLocationPermissionRequest(requestCode, activity);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        WisataItem list = items.get(position);

        Picasso.get().load(list.getImg_url()).into(holder.ivImgWisata);
        holder.tvTitleWisata.setText(list.getTitle());
        holder.tvKategoriWisata.setText(list.getKategori());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivImgWisata;
        public TextView tvTitleWisata, tvKategoriWisata;
        public Button btnDetailWisata, btnGetDirectionWisata;

        public ViewHolder(View itemView) {
            super(itemView);

            ivImgWisata = itemView.findViewById(R.id.img_wisata);
            tvTitleWisata = itemView.findViewById(R.id.title_wisata);
            tvKategoriWisata = itemView.findViewById(R.id.kategori_wisata);
            btnDetailWisata = itemView.findViewById(R.id.detail_wisata);
            btnGetDirectionWisata = itemView.findViewById(R.id.get_direction_wisata);
        }
    }
}