package com.gogresik.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.gogresik.R;
import com.gogresik.activity.DetailBerita;
import com.gogresik.model.BeritaItem;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CardBeritaAdapter extends RecyclerView.Adapter<CardBeritaAdapter.ViewHolder> {

    List<BeritaItem> items;

    public CardBeritaAdapter(List<BeritaItem> items) {
        super();
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_berita, parent, false);

        final ViewHolder viewHolder = new ViewHolder(v);

        viewHolder.ivImgBerita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), DetailBerita.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.putExtra("img", items.get(viewHolder.getAdapterPosition()).getUrl());
                i.putExtra("title", items.get(viewHolder.getAdapterPosition()).getTitle());
                i.putExtra("kategori", items.get(viewHolder.getAdapterPosition()).getKategori());
                i.putExtra("created_at", items.get(viewHolder.getAdapterPosition()).getCreated_at());
                i.putExtra("description", items.get(viewHolder.getAdapterPosition()).getDescription());

                v.getContext().startActivity(i);
            }
        });

        viewHolder.btnDetailBerita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), DetailBerita.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.putExtra("img", items.get(viewHolder.getAdapterPosition()).getUrl());
                i.putExtra("title", items.get(viewHolder.getAdapterPosition()).getTitle());
                i.putExtra("kategori", items.get(viewHolder.getAdapterPosition()).getKategori());
                i.putExtra("created_at", items.get(viewHolder.getAdapterPosition()).getCreated_at());
                i.putExtra("description", items.get(viewHolder.getAdapterPosition()).getDescription());

                v.getContext().startActivity(i);
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        BeritaItem list = items.get(position);

        Picasso.get().load(list.getUrl()).into(holder.ivImgBerita);
        holder.tvTitleBerita.setText(list.getTitle());
        holder.tvKategoriBerita.setText(list.getKategori());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView ivImgBerita;
        public TextView tvTitleBerita, tvKategoriBerita;
        public Button btnDetailBerita;

        public ViewHolder(View itemView) {
            super(itemView);

            ivImgBerita = itemView.findViewById(R.id.img_berita);
            tvTitleBerita = itemView.findViewById(R.id.title_berita);
            tvKategoriBerita = itemView.findViewById(R.id.kategori_berita);
            btnDetailBerita = itemView.findViewById(R.id.detail_berita);
        }
    }
}
