package com.gogresik.adapter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.gogresik.R;
import com.gogresik.activity.DetailWisata;
import com.gogresik.helper.PermissionUtils;
import com.gogresik.model.ReviewItem;
import com.gogresik.model.WisataItem;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CardReviewAdapter extends RecyclerView.Adapter<CardReviewAdapter.ViewHolder> {

    List<ReviewItem> items;

    public CardReviewAdapter(List<ReviewItem> items) {
        super();
        this.items = items;
    }

    @SuppressLint("MissingPermission")
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_review, parent, false);

        final ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ReviewItem list = items.get(position);

        holder.tvUserFullname.setText(list.getFullname());
        holder.tvCreatedAtReview.setText(list.getCreatedAt());
        holder.tvReviewDescription.setText(list.getDescription());
        holder.ratingBarReview.setRating(Float.parseFloat(list.getRating()));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvUserFullname, tvCreatedAtReview, tvReviewDescription;
        public AppCompatRatingBar ratingBarReview;

        public ViewHolder(View itemView) {
            super(itemView);

            tvUserFullname = itemView.findViewById(R.id.review_user_fullname);
            tvCreatedAtReview = itemView.findViewById(R.id.review_created_at);
            tvReviewDescription = itemView.findViewById(R.id.review_description);
            ratingBarReview = itemView.findViewById(R.id.review_rating_bar);
        }
    }
}
