package com.gogresik.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gogresik.R;
import com.gogresik.helper.Helper;
import com.gogresik.model.FeedCommentItem;
import com.gogresik.model.FeedItem;

import java.util.List;

public class CardFeedCommentAdapter extends RecyclerView.Adapter<CardFeedCommentAdapter.ViewHolder> {

    List<FeedCommentItem> items;

    public CardFeedCommentAdapter(List<FeedCommentItem> items) {
        super();
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_feed_comment, parent, false);

        final ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        FeedCommentItem list = items.get(position);

        holder.tvFullname.setText(list.getUserFullname());
        holder.tvCreatedAt.setText(Helper.dateFormatter(list.getCreatedAt()));
        holder.tvDescription.setText(list.getDescription());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvFullname, tvCreatedAt, tvDescription;

        public ViewHolder(View itemView) {
            super(itemView);

            tvFullname = itemView.findViewById(R.id.fc_fullname);
            tvCreatedAt = itemView.findViewById(R.id.fc_createdAt);
            tvDescription = itemView.findViewById(R.id.fc_description);
        }
    }
}