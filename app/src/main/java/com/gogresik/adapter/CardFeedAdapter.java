package com.gogresik.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.gogresik.R;
import com.gogresik.activity.DetailBerita;
import com.gogresik.activity.DetailWisata;
import com.gogresik.activity.FeedComment;
import com.gogresik.helper.Helper;
import com.gogresik.model.BeritaItem;
import com.gogresik.model.FeedItem;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CardFeedAdapter extends RecyclerView.Adapter<CardFeedAdapter.ViewHolder> {

    List<FeedItem> items;

    public CardFeedAdapter(List<FeedItem> items) {
        super();
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_feed, parent, false);

        final ViewHolder viewHolder = new ViewHolder(v);

        viewHolder.imageBtnComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(v.getContext(), FeedComment.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                i.putExtra("feed_id", items.get(viewHolder.getAdapterPosition()).getId());

                v.getContext().startActivity(i);
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        FeedItem list = items.get(position);

        holder.tvFullname.setText(list.getUserFullname());
        holder.tvCreatedAt.setText(Helper.dateFormatter(list.getCreatedAt()));
        holder.tvDescription.setText(list.getDescription());
        holder.tvTotalComment.setText(list.getTotal_comment());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvFullname, tvCreatedAt, tvDescription, tvTotalComment;
        public ImageButton imageBtnComment;

        public ViewHolder(View itemView) {
            super(itemView);

            tvFullname = itemView.findViewById(R.id.feed_fullname);
            tvCreatedAt = itemView.findViewById(R.id.feed_createdAt);
            tvDescription = itemView.findViewById(R.id.feed_description);
            imageBtnComment = itemView.findViewById(R.id.feed_comment_button);
            tvTotalComment = itemView.findViewById(R.id.feed_total_comment);
        }
    }
}