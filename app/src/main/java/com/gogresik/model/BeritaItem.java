package com.gogresik.model;

import android.graphics.Bitmap;

/**
 * Created by vikowijaya on 3/19/18.
 */

public class BeritaItem
{
    private String id;
    private String kategori;
    private String title;
    private String description;
    private String url;
    private String created_at;
    private boolean isSpinner;
    private Bitmap img;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isSpinner() {
        return isSpinner;
    }

    public void setSpinner(boolean spinner) {
        isSpinner = spinner;
    }

    public Bitmap getImg() {
        return img;
    }

    public void setImg(Bitmap img) {
        this.img = img;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
