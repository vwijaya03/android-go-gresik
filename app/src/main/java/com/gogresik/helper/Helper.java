package com.gogresik.helper;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.gogresik.R;
import com.gogresik.activity.Berita;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static android.content.Context.MODE_PRIVATE;

public class Helper extends Activity{

    // Shared Preferences
    private static SharedPreferences mSharedPreferences;
    private static TextView mTextViewDrawerHeaderFullname;
    private static TextView mTextViewDrawerHeaderEmail;
    private static ImageView mImageViewDrawerHeaderImg;

    // Google Sign In Client
    private GoogleSignInClient mGoogleSignInClient;

    public static final String PROTOCOL = "http://";
//    public static final String HOST = "10.212.91.101/gogresik/public";
    public static final String HOST = "192.168.1.103/gogresik/public";
//    public static final String HOST = "172.20.10.5/gogresik/public";
    public static final String PREFIX = "api";

    //Google Sign In API
    public static final String GOOGLE_LOGIN = "google-signin";
    //Google Sign In Url
    public static final String POST_GOOGLE_LOGIN = PROTOCOL+HOST+'/'+PREFIX+'/'+GOOGLE_LOGIN;

    //Berita API
    public static final String BERITA = "berita";
    //Berita Url
    public static final String GET_BERITA = PROTOCOL+HOST+'/'+PREFIX+'/'+BERITA;

    //Berita API
    public static final String SEARCH_BERITA = "search-berita";
    //Berita Url
    public static final String GET_SEARCH_BERITA = PROTOCOL+HOST+'/'+PREFIX+'/'+SEARCH_BERITA;

    //Search Event & Event Url
    public static final String GET_EVENT = PROTOCOL+HOST+'/'+PREFIX+'/'+"event";
    public static final String GET_SEARCH_EVENT = PROTOCOL+HOST+'/'+PREFIX+'/'+"search-event";

    //Search Wisata & Wisata Url
    public static final String GET_WISATA = PROTOCOL+HOST+'/'+PREFIX+'/'+"wisata";
    public static final String GET_WISATA_REVIEW = PROTOCOL+HOST+'/'+PREFIX+'/'+"wisata-all-review";
    public static final String GET_WISATA_RATING = PROTOCOL+HOST+'/'+PREFIX+'/'+"wisata-rating";
    public static final String POST_ADD_REVIEW_WISATA = PROTOCOL+HOST+'/'+PREFIX+'/'+"add-review-wisata";
    public static final String GET_SEARCH_WISATA = PROTOCOL+HOST+'/'+PREFIX+'/'+"search-wisata";

    // Information Public
    public static final String GET_INFORMATION_PUBLIC = PROTOCOL+HOST+'/'+PREFIX+'/'+"information-public";

    // Feed
    public static final String POST_ADD_NEW_FEED = PROTOCOL+HOST+'/'+PREFIX+'/'+"add-new-feed";
    public static final String GET_FEED = PROTOCOL+HOST+'/'+PREFIX+'/'+"feed";
    public static final String GET_FEED_COMMENT = PROTOCOL+HOST+'/'+PREFIX+'/'+"feed-comment/";
    public static final String POST_ADD_FEED_COMMENT = PROTOCOL+HOST+'/'+PREFIX+'/'+"add-feed-comment";

    public static void setProfile(Activity currentActivity, View view) {

        mSharedPreferences = currentActivity.getSharedPreferences("AuthUser", MODE_PRIVATE);

        TextView TextViewDrawerHeaderFullname = view.findViewById(R.id.drawer_header_fullname);
        mTextViewDrawerHeaderEmail =  view.findViewById(R.id.drawer_header_email);
        mImageViewDrawerHeaderImg = view.findViewById(R.id.drawer_header_img);

        Picasso.get().load(mSharedPreferences.getString("AuthUserImage", "")).into(mImageViewDrawerHeaderImg);
        TextViewDrawerHeaderFullname.setText(mSharedPreferences.getString("AuthUserFullname", ""));
        mTextViewDrawerHeaderEmail.setText(mSharedPreferences.getString("AuthUserEmail", ""));
    }

    public static GoogleSignInClient signOut(Activity currentActivity) {

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(currentActivity.getString(R.string.server_client_id))
                .requestEmail()
                .build();

        GoogleSignInClient mGoogleSignInClient = GoogleSignIn.getClient(currentActivity, gso);

        mSharedPreferences = currentActivity.getSharedPreferences("AuthUser", MODE_PRIVATE);
        mSharedPreferences.edit().clear().apply();

        return mGoogleSignInClient;
    }

    public static String stripHtml(String html) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY).toString();
        } else {
            return Html.fromHtml(html).toString();
        }
    }

    public Bitmap scaleToActualAspectRatio(Bitmap image, Activity activity) {

        DisplayMetrics displaymetrics = new DisplayMetrics();

        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int maxWidth = displaymetrics.widthPixels;
        int maxHeight = displaymetrics.heightPixels;

//        Display display = getWindowManager().getDefaultDisplay();
//
//        // display size in pixels
//        Point size = new Point();
//        display.getSize(size);
//        int maxWidth = size.x;
//        int maxHeight = size.y;

        //activity.getWindowManager().getDefaultDisplay().getWidth();

        if (maxHeight > 0 && maxWidth > 0) {
            int width = image.getWidth();
            int height = image.getHeight();
            float ratioBitmap = (float) width / (float) height;
            float ratioMax = (float) maxWidth / (float) maxHeight;

            int finalWidth = maxWidth;
            int finalHeight = maxHeight;
            if (ratioMax > 1) {
                finalWidth = (int) ((float)maxHeight * ratioBitmap);
            } else {
                finalHeight = (int) ((float)maxWidth / ratioBitmap);
            }
            image = Bitmap.createScaledBitmap(image, finalWidth, finalHeight, true);
            return image;
        } else {
            return image;
        }
    }

    public String saveResizedImageAfterTakenFromCameraToInternalStorage(Bitmap bitmap, Activity activity)
    {
        String absolutePath = "";
        Bitmap modifiedBitmap;
        // Start Buat Image Baru Yang Di Compress lalu disimpan di internal storage
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),"DCIM");
        if (!file.exists()) {
            file.mkdirs();
        }

        File localFile = new File(file + File.separator + "IMG_Resized_" + String.valueOf(System.currentTimeMillis()) + ".jpg");

        try {

            OutputStream fOut = new FileOutputStream(localFile);

            modifiedBitmap = modifyOrientation(bitmap, absolutePath);

            scaleToActualAspectRatio(modifiedBitmap, activity).compress(Bitmap.CompressFormat.JPEG, 80, fOut);

            MediaStore.Images.Media.insertImage(activity.getContentResolver(), localFile.getAbsolutePath(), localFile.getName(), localFile.getName());
            absolutePath = localFile.getAbsolutePath();

            fOut.flush();
            fOut.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // END Buat Image Baru Yang Di Compress lalu disimpan di internal storage

        return absolutePath;
    }

    public String getRealPathFromURI(Uri uri){
        String filePath = "";
        String[] filePahColumn = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(uri, filePahColumn, null, null, null);
        if (cursor != null) {
            if(cursor.moveToFirst()){
                int columnIndex = cursor.getColumnIndex(filePahColumn[0]);
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        }
        return filePath;
    }

    public static Bitmap modifyOrientation(Bitmap bitmap, String image_absolute_path) throws IOException {
        ExifInterface ei = new ExifInterface(image_absolute_path);
        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotate(bitmap, 90);

            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotate(bitmap, 180);

            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotate(bitmap, 270);

            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                return flip(bitmap, true, false);

            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                return flip(bitmap, false, true);

            default:
                return bitmap;
        }
    }

    public static Bitmap rotate(Bitmap bitmap, float degrees) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degrees);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public static Bitmap flip(Bitmap bitmap, boolean horizontal, boolean vertical) {
        Matrix matrix = new Matrix();
        matrix.preScale(horizontal ? -1 : 1, vertical ? -1 : 1);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    public static String dateFormatter(String date) {

        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy HH:mm");

        Date output_date = null;
        try {
            output_date = inputFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String outputDateStr = outputFormat.format(output_date);

        return outputDateStr;
    }

    public static long convertToMilisecond(String date) {
        DateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat inputFormat = new SimpleDateFormat("dd MMMM yyyy");

        Date output_date = null;
        try {
            output_date = inputFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return output_date.getTime();
    }

    public static void errorAlert(final Activity currentActivity) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(currentActivity);
        alertDialog.setTitle("Error");
        alertDialog.setMessage("Terjadi kesalahan");
        alertDialog.setCancelable(false);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                currentActivity.finish();
                System.exit(0);
            }
        });
        alertDialog.show();
    }
}
