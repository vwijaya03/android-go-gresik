package com.gogresik.helper;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.widget.AbsListView;


public abstract class EndlessRecyclerOnScrollListener extends RecyclerView.OnScrollListener {

    LinearLayoutManager layoutManager;

    /**
     * Supporting only LinearLayoutManager for now.
     *
     * @param layoutManager
     */
    public EndlessRecyclerOnScrollListener(LinearLayoutManager layoutManager) {
        this.layoutManager = layoutManager;
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        int visibleItemCount = layoutManager.getChildCount();
        int totalItemCount = layoutManager.getItemCount();
        int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

//        Log.i("first", String.valueOf(firstVisibleItemPosition));
//        Log.i("sum", String.valueOf(visibleItemCount+firstVisibleItemPosition));
//        Log.i("total", String.valueOf(totalItemCount));
//        Log.i("islOad", String.valueOf(isLoading()));
//        Log.i("isLastPage", String.valueOf(isLastPage()));

        int maxVisibleItemCount = 0;

        if(visibleItemCount == 3) {
            maxVisibleItemCount = visibleItemCount - 2;
        } else {
            maxVisibleItemCount = visibleItemCount;
        }

        if (!isLoading() && !isLastPage()) {
            if ((maxVisibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0) {
                loadMoreItems();
            }
        }

    }

    protected abstract void loadMoreItems();

    public abstract int getTotalPageCount();

    public abstract boolean isLastPage();

    public abstract boolean isLoading();

}