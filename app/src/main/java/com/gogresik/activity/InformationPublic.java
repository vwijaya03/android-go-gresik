package com.gogresik.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.gogresik.R;
import com.gogresik.adapter.CardBeritaAdapter;
import com.gogresik.adapter.CardInformationPublicAdapter;
import com.gogresik.helper.AsyncReuse;
import com.gogresik.helper.EndlessRecyclerOnScrollListener;
import com.gogresik.helper.GetResponse;
import com.gogresik.helper.Helper;
import com.gogresik.model.BeritaItem;
import com.gogresik.model.InformationPublicItem;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.gogresik.helper.Helper.GET_BERITA;
import static com.gogresik.helper.Helper.GET_INFORMATION_PUBLIC;
import static com.gogresik.helper.Helper.GET_SEARCH_BERITA;

public class InformationPublic extends AppCompatActivity implements GetResponse
{
    private ActionBar actionBar;
    private Toolbar toolbar;
    private ProgressBar mProgressBarBerita;

    // Recycler View
    private RecyclerView mRecyclerViewInformationPublic;
    private RecyclerView.Adapter mAdapter;

    // Swipe Refresh
    private SwipeRefreshLayout mSwipeRefreshLayoutInformationPublic;

    // Layout Manager
    private LinearLayoutManager mLinearLayoutManager;

    // Async
    private AsyncReuse requestServer;

    private String TAG = "Information Public";
    private String mQuery = "";
    private int mNextPage = 1;
    private int mTotalPages = 0;
    private int mCurrentPages = 0;
    private int mLastPages = 0;
    private boolean mIsLoading = false;
    private boolean mIsLastPage = false;
    private List<InformationPublicItem> mDataListInformationPublic = new ArrayList<InformationPublicItem>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information_public);

        initToolbar();
        initNavigationMenu();

        mSwipeRefreshLayoutInformationPublic = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout_information_public);
        mRecyclerViewInformationPublic = findViewById(R.id.recyclerViewInformationPublic);
        mRecyclerViewInformationPublic.setHasFixedSize(false);

        mLinearLayoutManager = new LinearLayoutManager(this);

        mRecyclerViewInformationPublic.setLayoutManager(mLinearLayoutManager);

        // On Refresh
        mSwipeRefreshLayoutInformationPublic.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mIsLastPage = false;
                mIsLoading = false;
                mQuery = "";
                mNextPage = 1;
                executeServerReq(GET_INFORMATION_PUBLIC);
                requestServer.getObjectQ("");
                requestServer.getRequestMethod("GET");
                requestServer.setDialogtext("Loading");
                requestServer.execute();
                mSwipeRefreshLayoutInformationPublic.setRefreshing(false);
            }
        });

        // On Scroll Down
        mRecyclerViewInformationPublic.addOnScrollListener(new EndlessRecyclerOnScrollListener(mLinearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                mIsLoading = true;

                mNextPage += 1;

                executeServerReq(GET_INFORMATION_PUBLIC);
                requestServer.getObjectQ("page="+mNextPage);
                requestServer.getRequestMethod("GET");
                requestServer.setDialogtext("Loading");
                requestServer.execute();
            }

            @Override
            public int getTotalPageCount() {
                return mTotalPages;
            }

            @Override
            public boolean isLastPage() {
                return mIsLastPage;
            }

            @Override
            public boolean isLoading() {
                return mIsLoading;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(mDataListInformationPublic.size() > 0) {
            mDataListInformationPublic.clear();
            mIsLoading = false;
            mIsLastPage = false;
            mQuery = "";
            mNextPage = 1;

            executeServerReq(GET_INFORMATION_PUBLIC);
            requestServer.getObjectQ("");
            requestServer.getRequestMethod("GET");
            requestServer.setDialogtext("Loading");
            requestServer.execute();
        } else {
            executeServerReq(GET_INFORMATION_PUBLIC);
            requestServer.getObjectQ("");
            requestServer.getRequestMethod("GET");
            requestServer.setDialogtext("Loading");
            requestServer.execute();
        }

    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle("Information Public");
    }

    private void initNavigationMenu() {
        NavigationView nav_view = (NavigationView) findViewById(R.id.nav_view);
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(final MenuItem item) {
                if(item.getItemId() == R.id.nav_logout) {
                    Helper.signOut(InformationPublic.this).signOut().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Intent i = new Intent(getApplicationContext(), Login.class);
                            startActivity(i);
                        }
                    });
                } else if(item.getItemId() == R.id.nav_berita) {
                    Intent i = new Intent(getApplicationContext(), Berita.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);

                } else if(item.getItemId() == R.id.nav_event) {
                    Intent i = new Intent(getApplicationContext(), Event.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                } else if(item.getItemId() == R.id.nav_wisata) {
                    Intent i = new Intent(getApplicationContext(), Wisata.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                } else if(item.getItemId() == R.id.nav_feed) {
                    Intent i = new Intent(getApplicationContext(), Feed.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                } else if(item.getItemId() == R.id.nav_informasi_public) {
                    Intent i = new Intent(getApplicationContext(), InformationPublic.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                }
                //Toast.makeText(getApplicationContext(), item.getTitle() + " Selected", Toast.LENGTH_SHORT).show();
                //actionBar.setTitle(item.getTitle());
                drawer.closeDrawers();
                return true;
            }
        });

        View viewNavDrawerHeader = nav_view.getHeaderView(0);

        Helper.setProfile(this, viewNavDrawerHeader);
    }

    private void executeServerReq(String url) {
        Log.i(TAG, url);
        requestServer = new AsyncReuse(url, true, this, getApplicationContext());
        requestServer.getResponse = this;
    }

    @Override
    public void getData(String response) {
        try {
            Log.i("response", String.valueOf(mNextPage));
            JSONObject beritaJsonObject = new JSONObject(response.toString());
            JSONArray beritaArray = beritaJsonObject.getJSONArray("data");

            mCurrentPages = Integer.parseInt(beritaJsonObject.getString("current_page"));
            mLastPages = Integer.parseInt(beritaJsonObject.getString("last_page"));
            mTotalPages = Integer.parseInt(beritaJsonObject.getString("total"));

            if(mCurrentPages >= mLastPages) {
                mIsLastPage = true;
            }

            mIsLoading = false;

            if(mNextPage == 1) {
                this.mDataListInformationPublic.removeAll(this.mDataListInformationPublic);
                mAdapter = new CardInformationPublicAdapter(this.mDataListInformationPublic);
                mRecyclerViewInformationPublic.setAdapter(mAdapter);
            }

            for(int i=0; i < beritaArray.length(); i++){
                InformationPublicItem item = new InformationPublicItem();
                JSONObject j = beritaArray.getJSONObject(i);

                item.setId(j.getString("id"));
                item.setTitle(j.getString("title"));
                item.setDescription(j.getString("description"));
                item.setCreatedAt(j.getString("created_at"));

                this.mDataListInformationPublic.add(item);
            }

            mAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            Helper.errorAlert(this);
            e.printStackTrace();
        }

    }
}
