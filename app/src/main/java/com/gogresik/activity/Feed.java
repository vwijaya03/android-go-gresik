package com.gogresik.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gogresik.R;
import com.gogresik.adapter.CardFeedAdapter;
import com.gogresik.adapter.CardWisataAdapter;
import com.gogresik.helper.AsyncReuse;
import com.gogresik.helper.EndlessRecyclerOnScrollListener;
import com.gogresik.helper.GetResponse;
import com.gogresik.helper.Helper;
import com.gogresik.helper.PermissionUtils;
import com.gogresik.model.FeedItem;
import com.gogresik.model.WisataItem;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.gogresik.helper.Helper.GET_BERITA;
import static com.gogresik.helper.Helper.GET_FEED;
import static com.gogresik.helper.Helper.GET_SEARCH_BERITA;
import static com.gogresik.helper.Helper.GET_SEARCH_WISATA;
import static com.gogresik.helper.Helper.GET_WISATA;
import static com.gogresik.helper.Helper.POST_ADD_NEW_FEED;
import static com.gogresik.helper.Helper.POST_ADD_REVIEW_WISATA;

public class Feed extends AppCompatActivity implements GetResponse
{
    private ActionBar actionBar;
    private Toolbar toolbar;
    private ProgressBar mProgressBarBerita;
    private TextView mTextViewCurrentUserName;

    // Recycler View
    private RecyclerView mRecyclerViewFeed;
    private RecyclerView.Adapter mAdapter;

    // Swipe Refresh
    private SwipeRefreshLayout mSwipeRefreshLayoutFeed;

    // Layout Manager
    private LinearLayoutManager mLinearLayoutManager;

    // Async
    private AsyncReuse requestServer;

    // Shared Preferences
    private SharedPreferences mSharedPreferences;

    // list of permissions
    private ArrayList<String> permissions=new ArrayList<>();
    private PermissionUtils permissionUtils;
    private boolean isPermissionGranted;

    private String TAG = "Feed";
    private String mQuery = "";
    private int mNextPage = 1;
    private int mTotalPages = 0;
    private int mCurrentPages = 0;
    private int mLastPages = 0;
    private boolean mIsLoading = false;
    private boolean mIsLastPage = false;
    private List<FeedItem> mDataListFeed = new ArrayList<FeedItem>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed);

        initToolbar();
        initNavigationMenu();

        mSwipeRefreshLayoutFeed = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout_feed);
        mRecyclerViewFeed = findViewById(R.id.recyclerViewFeed);
        mRecyclerViewFeed.setHasFixedSize(false);

        mLinearLayoutManager = new LinearLayoutManager(this);

        mRecyclerViewFeed.setLayoutManager(mLinearLayoutManager);

        // On Refresh
        mSwipeRefreshLayoutFeed.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mIsLastPage = false;
                mIsLoading = false;
                mQuery = "";
                mNextPage = 1;

                executeServerReq(GET_FEED);
                requestServer.getObjectQ("");
                requestServer.getRequestMethod("GET");
                requestServer.setDialogtext("Loading");
                requestServer.execute();
                mSwipeRefreshLayoutFeed.setRefreshing(false);
            }
        });


        // On Scroll Down
        mRecyclerViewFeed.addOnScrollListener(new EndlessRecyclerOnScrollListener(mLinearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                mIsLoading = true;

                mNextPage += 1;
                if(mQuery != "") {
                    executeServerReq(GET_SEARCH_WISATA);
                    requestServer.getObjectQ("page="+mNextPage);
                    requestServer.getRequestMethod("GET");
                    requestServer.setHeaderKeyAndValue("search", mQuery);
                    requestServer.setDialogtext("Loading");
                    requestServer.execute();
                } else {
                    executeServerReq(GET_WISATA);
                    requestServer.getObjectQ("page="+mNextPage);
                    requestServer.getRequestMethod("GET");
                    requestServer.setDialogtext("Loading");
                    requestServer.execute();
                }

            }

            @Override
            public int getTotalPageCount() {
                return mTotalPages;
            }

            @Override
            public boolean isLastPage() {
                return mIsLastPage;
            }

            @Override
            public boolean isLoading() {
                return mIsLoading;
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(mDataListFeed.size() > 0) {
            mDataListFeed.clear();
            mIsLoading = false;
            mIsLastPage = false;
            mQuery = "";
            mNextPage = 1;

            executeServerReq(GET_FEED);
            requestServer.getObjectQ("");
            requestServer.getRequestMethod("GET");
            requestServer.setDialogtext("Loading");
            requestServer.execute();
        } else {
            executeServerReq(GET_FEED);
            requestServer.getObjectQ("");
            requestServer.getRequestMethod("GET");
            requestServer.setDialogtext("Loading");
            requestServer.execute();
        }
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle("Feed");

        mSharedPreferences = getSharedPreferences("AuthUser", MODE_PRIVATE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.option_menu_new_post, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.new_feed) {
            showCustomDialog();
        }

        return true;
    }

    private void showCustomDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_add_new_feed);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        final AppCompatButton bt_submit = (AppCompatButton) dialog.findViewById(R.id.bt_submit);
        mTextViewCurrentUserName = dialog.findViewById(R.id.add_new_feed_current_user_name);

        mTextViewCurrentUserName.setText(mSharedPreferences.getString("AuthUserFullname", ""));

        ((EditText) dialog.findViewById(R.id.et_post)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                bt_submit.setEnabled(!s.toString().trim().isEmpty());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        bt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                //Toast.makeText(getApplicationContext(), "Post Submitted", Toast.LENGTH_SHORT).show();

                requestServer = new AsyncReuse(POST_ADD_NEW_FEED, true, Feed.this, getApplicationContext());
                requestServer.getResponse = Feed.this;
                requestServer.getObjectQ("user_id="+mSharedPreferences.getString("AuthUserId", "")+
                        "&description="+((EditText) dialog.findViewById(R.id.et_post)).getText());
                requestServer.getRequestMethod("POST");
                requestServer.setDialogtext("Loading");
                requestServer.execute();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    private void initNavigationMenu() {
        NavigationView nav_view = (NavigationView) findViewById(R.id.nav_view);
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(final MenuItem item) {
                if(item.getItemId() == R.id.nav_logout) {
                    Helper.signOut(Feed.this).signOut().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Intent i = new Intent(getApplicationContext(), Login.class);
                            startActivity(i);
                        }
                    });
                } else if(item.getItemId() == R.id.nav_berita) {
                    Intent i = new Intent(getApplicationContext(), Berita.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);

                } else if(item.getItemId() == R.id.nav_event) {
                    Intent i = new Intent(getApplicationContext(), Event.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                } else if(item.getItemId() == R.id.nav_wisata) {
                    Intent i = new Intent(getApplicationContext(), Wisata.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                } else if(item.getItemId() == R.id.nav_feed) {
                    Intent i = new Intent(getApplicationContext(), Feed.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                } else if(item.getItemId() == R.id.nav_informasi_public) {
                    Intent i = new Intent(getApplicationContext(), InformationPublic.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                }
                //Toast.makeText(getApplicationContext(), item.getTitle() + " Selected", Toast.LENGTH_SHORT).show();
                //actionBar.setTitle(item.getTitle());
                drawer.closeDrawers();
                return true;
            }
        });

        View viewNavDrawerHeader = nav_view.getHeaderView(0);

        Helper.setProfile(this, viewNavDrawerHeader);
    }

    private void executeServerReq(String url) {
        Log.i(TAG, url);
        requestServer = new AsyncReuse(url, true, this, getApplicationContext());
        requestServer.getResponse = this;
    }

    @Override
    public void getData(String response) {
        try {
            Log.i("response", String.valueOf(mNextPage));
            JSONObject jsonObject = new JSONObject(response.toString());

            if("add_feed".equals(jsonObject.getString("type"))) {
                Log.i("masuk add", "masuk add");
                if("success".equals(jsonObject.getString("result"))) {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                    alertDialog.setTitle("Success");
                    alertDialog.setCancelable(false);
                    alertDialog.setMessage("Feed berhasil di tambahkan");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            mIsLastPage = false;
                            mIsLoading = false;
                            mQuery = "";
                            mNextPage = 1;

                            executeServerReq(GET_FEED);
                            requestServer.getObjectQ("");
                            requestServer.getRequestMethod("GET");
                            requestServer.setDialogtext("Loading");
                            requestServer.execute();
                        }
                    });
                    alertDialog.show();
                } else {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                    alertDialog.setTitle("Gagal");
                    alertDialog.setMessage("Silahkan coba lagi");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                    alertDialog.show();
                }
            } else {

                JSONArray feedArray = jsonObject.getJSONObject("result").getJSONArray("data");

                mCurrentPages = Integer.parseInt(jsonObject.getJSONObject("result").getString("current_page"));
                mLastPages = Integer.parseInt(jsonObject.getJSONObject("result").getString("last_page"));
                mTotalPages = Integer.parseInt(jsonObject.getJSONObject("result").getString("total"));

                if(mCurrentPages >= mLastPages) {
                    mIsLastPage = true;
                }

                mIsLoading = false;

                if(mNextPage == 1) {
                    this.mDataListFeed.removeAll(this.mDataListFeed);
                    mAdapter = new CardFeedAdapter(this.mDataListFeed);
                    mRecyclerViewFeed.setAdapter(mAdapter);
                }

                for(int i=0; i < feedArray.length(); i++){
                    FeedItem item = new FeedItem();
                    JSONObject j = feedArray.getJSONObject(i);

                    item.setId(j.getString("id"));
                    item.setUserFullname(j.getString("fullname"));
                    item.setCreatedAt(j.getString("created_at"));
                    item.setDescription(j.getString("description"));
                    item.setTotal_comment(j.getString("total_comment"));

                    this.mDataListFeed.add(item);
                }

                mAdapter.notifyDataSetChanged();
            }

        } catch (JSONException e) {
            Helper.errorAlert(this);
            e.printStackTrace();
        }

    }
}
