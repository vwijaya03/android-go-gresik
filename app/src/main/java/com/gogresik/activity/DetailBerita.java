package com.gogresik.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.gogresik.R;
import com.gogresik.helper.Helper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.squareup.picasso.Picasso;

public class DetailBerita extends AppCompatActivity {

    private ActionBar actionBar;
    private Toolbar toolbar;
    private ImageView mDetailBeritaImage;
    private TextView mDetailBeritaKategori, mDetailBeritaTitle, mDetailBeritaCreatedAt, mDetailBeritaDescription;
    private String mImgUrl, mTitle, mKategori, mCreatedAt, mDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_berita);

        initToolbar(savedInstanceState);
        initNavigationMenu();

        Picasso.get().load(mImgUrl).into(mDetailBeritaImage);
        mDetailBeritaKategori.setText(mKategori);
        mDetailBeritaTitle.setText(mTitle);
        mDetailBeritaCreatedAt.setText(Helper.dateFormatter(mCreatedAt));
        mDetailBeritaDescription.setText(mDescription);
    }

    private void initToolbar(Bundle savedInstanceState) {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(null);

        mDetailBeritaImage = findViewById(R.id.detail_berita_image);
        mDetailBeritaKategori = findViewById(R.id.detail_berita_kategori);
        mDetailBeritaTitle = findViewById(R.id.detail_berita_title);
        mDetailBeritaCreatedAt = findViewById(R.id.detail_berita_created_at);
        mDetailBeritaDescription = findViewById(R.id.detail_berita_description);

        if(savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();

            if(extras == null)
            {
                this.mImgUrl = null;
                this.mKategori = null;
                this.mTitle = null;
                this.mCreatedAt = null;
                this.mDescription = null;
            } else {
                this.mImgUrl = extras.getString("img");
                this.mKategori = extras.getString("kategori");
                this.mTitle = extras.getString("title");
                this.mCreatedAt = extras.getString("created_at");
                this.mDescription = extras.getString("description");
            }
        } else {
            this.mImgUrl = (String) savedInstanceState.getSerializable("img");
            this.mKategori = (String) savedInstanceState.getSerializable("kategori");
            this.mTitle = (String) savedInstanceState.getSerializable("title");
            this.mCreatedAt = (String) savedInstanceState.getSerializable("created_at");
            this.mDescription = (String) savedInstanceState.getSerializable("description");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        return false;
    }

    private void initNavigationMenu() {
        NavigationView nav_view = (NavigationView) findViewById(R.id.nav_view);
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(final MenuItem item) {
                if(item.getItemId() == R.id.nav_logout) {
                    Helper.signOut(DetailBerita.this).signOut().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Intent i = new Intent(getApplicationContext(), Login.class);
                            startActivity(i);
                        }
                    });
                } else if(item.getItemId() == R.id.nav_berita) {
                    Intent i = new Intent(getApplicationContext(), Berita.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                } else if(item.getItemId() == R.id.nav_event) {
                    Intent i = new Intent(getApplicationContext(), Event.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                } else if(item.getItemId() == R.id.nav_wisata) {
                    Intent i = new Intent(getApplicationContext(), Wisata.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                } else if(item.getItemId() == R.id.nav_informasi_public) {
                    Intent i = new Intent(getApplicationContext(), InformationPublic.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                } else if(item.getItemId() == R.id.nav_feed) {
                    Intent i = new Intent(getApplicationContext(), Feed.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                }
                //Toast.makeText(getApplicationContext(), item.getTitle() + " Selected", Toast.LENGTH_SHORT).show();
                //actionBar.setTitle(item.getTitle());
                drawer.closeDrawers();
                return true;
            }
        });

        View viewNavDrawerHeader = nav_view.getHeaderView(0);

        Helper.setProfile(this, viewNavDrawerHeader);

        // open drawer at start
        //drawer.openDrawer(GravityCompat.START);
    }
}
