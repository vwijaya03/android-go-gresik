package com.gogresik.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import com.gogresik.R;
import com.gogresik.helper.AsyncReuse;
import com.gogresik.helper.CheckInternet;
import com.gogresik.helper.GetResponse;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.gogresik.helper.Helper;

import org.json.JSONException;
import org.json.JSONObject;

import static com.gogresik.helper.Helper.POST_GOOGLE_LOGIN;

public class Login extends AppCompatActivity implements GetResponse {

    // UI references.
    private SignInButton mSignInButton;

    // Google SignIn Account
    private GoogleSignInAccount mGoogleSignInAccount;

    // Google Signin Client
    private GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 100;
    private final int REQUEST_CODE_ASK_CAMERA_PERMISSION = 3000;
    private String TAG = "Login";

    // Async
    private AsyncReuse requestServer;
    private CheckInternet checkInternet;

    // Shared Preferences
    private SharedPreferences mSharedPreferences;

    // Progress Dialog
    private ProgressDialog mProgressDialog;

    private int mIsAuth = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mSignInButton = findViewById(R.id.sign_in_button);

        TextView textView = (TextView) mSignInButton.getChildAt(0);
        textView.setText("Login Dengan Google Account");

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_client_id))
                .requestEmail()
                .build();

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        mSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });

        if (!this.checkCameraPermission()) {
            this.requestCameraPermission();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.

            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);
        }
    }

    private void handleSignInResult(@NonNull Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);
            Log.i("GSignIn", account.getIdToken());
            // Signed in successfully, show authenticated UI.

            mGoogleSignInAccount = account;

            executeServerReq();
            requestServer.getObjectQ("email="+account.getEmail()+"&google_token="+account.getIdToken()+"&server_client_id="+getString(R.string.server_client_id));
            requestServer.getRequestMethod("POST");
            requestServer.setDialogtext("Loading");
            requestServer.execute();
        } catch (ApiException e) {

            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("GSignIn", "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }
    }

    private void updateUI(@Nullable GoogleSignInAccount account) {
        if (account != null) {
            mProgressDialog = new ProgressDialog(Login.this);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("Loading");
            mProgressDialog.show();
            mIsAuth = 1;

            executeServerReq();
            requestServer.getObjectQ("email="+account.getEmail()+"&google_token="+account.getIdToken()+"&server_client_id="+getString(R.string.server_client_id));
            requestServer.getRequestMethod("POST");
            requestServer.setDialogtext("Loading");
            requestServer.execute();
            //Intent i = new Intent(this, );
            //Log.i(TAG, "di update UI");
            //Log.i(TAG, String.valueOf(account.getIdToken()));
            //Intent i = new Intent(Login.this, Berita.class);
            //startActivity(i);
        } else {

        }
    }

    private void executeServerReq() {
        Log.i(TAG, POST_GOOGLE_LOGIN);
        requestServer = new AsyncReuse(POST_GOOGLE_LOGIN, true, this, getApplicationContext());
        requestServer.getResponse = this;
    }

    @Override
    public void getData(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response.toString());
            Log.e("here", "----------------" + jsonObject.toString());
            //TextView textView = (TextView) findViewById(R.id.opt);
            //textView.setText(jsonObject.toString());

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(Login.this);

            if(jsonObject.get("success" ).equals("ok")) {

                if(mIsAuth == 1) {
                    mProgressDialog.dismiss();
                }

                mSharedPreferences = getSharedPreferences("AuthUser", MODE_PRIVATE);
                mSharedPreferences.edit().putString("AuthUserId", jsonObject.getString("user_id")).apply();
                mSharedPreferences.edit().putString("AuthUserEmail", jsonObject.getString("email")).apply();
                mSharedPreferences.edit().putString("AuthUserFullname", jsonObject.getString("fullname")).apply();
                mSharedPreferences.edit().putString("AuthUserImage", jsonObject.getString("img_url")).apply();

                alertDialog.setTitle("Login Berhasil");
                alertDialog.setMessage(jsonObject.getString("result"));
                alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                alertDialog.setCancelable(false);
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(Login.this, Berita.class);
                        startActivity(i);
                    }
                });
                alertDialog.show();

            } else {
                Helper.signOut(this).signOut().addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Intent i = new Intent(getApplicationContext(), Login.class);
                        startActivity(i);
                    }
                });
            }
        } catch (JSONException e) {
            mProgressDialog.dismiss();
            Helper.errorAlert(this);
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        updateUI(account);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    private boolean checkCameraPermission() {
        boolean camera = (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED);
        boolean storage = (ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        return camera && storage;
    }

    private void requestCameraPermission(){
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_CODE_ASK_CAMERA_PERMISSION);
    }

    @Override
    public void onRequestPermissionsResult (int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_CAMERA_PERMISSION: {
                if (!this.checkCameraPermission()) {
                    android.support.v7.app.AlertDialog.Builder alertDialog = new android.support.v7.app.AlertDialog.Builder(this);

                    alertDialog.setTitle("Error");
                    alertDialog.setMessage("Membutuhkan izin mengakses camera / galeri.");
                    alertDialog.setCancelable(false);
                    alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                }
            } break;
        }
    }
}
