package com.gogresik.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatRatingBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.gogresik.R;
import com.gogresik.adapter.CardBeritaAdapter;
import com.gogresik.adapter.CardReviewAdapter;
import com.gogresik.helper.AsyncReuse;
import com.gogresik.helper.GetResponse;
import com.gogresik.helper.Helper;
import com.gogresik.model.BeritaItem;
import com.gogresik.model.ReviewItem;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import static com.gogresik.helper.Helper.GET_BERITA;
import static com.gogresik.helper.Helper.GET_WISATA_RATING;
import static com.gogresik.helper.Helper.GET_WISATA_REVIEW;
import static com.gogresik.helper.Helper.POST_ADD_REVIEW_WISATA;

public class DetailWisata extends AppCompatActivity implements GetResponse {

    private ActionBar actionBar;
    private Toolbar toolbar;
    private ImageView mDetailWisataImage;
    private TextView mDetailWisataTitle, mDetailWisataCreatedAt, mDetailWisataDescription;
    private AppCompatRatingBar mDetailWisataRatingBar;
    private FloatingActionButton mFabWriteReviewDetailWisata;

    private String mId, mImgUrl, mTitle, mRating, mCreatedAt, mDescription;

    private AsyncReuse requestServer;

    private SharedPreferences mSharedPreferences;

    private List<ReviewItem> mDataListReviewWisata = new ArrayList<ReviewItem>();

    // Recycler View
    private RecyclerView mRecyclerViewReview;
    private RecyclerView.Adapter mAdapter;

    // Layout Manager
    private LinearLayoutManager mLinearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_wisata);

        initToolbar(savedInstanceState);
        initNavigationMenu();
        Picasso.get().load(mImgUrl).into(mDetailWisataImage);
        mDetailWisataTitle.setText(mTitle);
        mDetailWisataCreatedAt.setText(Helper.dateFormatter(mCreatedAt));
        mDetailWisataDescription.setText(mDescription);

        mFabWriteReviewDetailWisata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCustomDialog();
            }
        });

        requestServer = new AsyncReuse(GET_WISATA_RATING, true, DetailWisata.this, getApplicationContext());
        requestServer.getResponse = DetailWisata.this;
        requestServer.getObjectQ("wisata_id="+mId);
        requestServer.getRequestMethod("POST");
        requestServer.setDialogtext("Loading");
        requestServer.execute();

        requestServer = new AsyncReuse(GET_WISATA_REVIEW, true, DetailWisata.this, getApplicationContext());
        requestServer.getResponse = DetailWisata.this;
        requestServer.getObjectQ("wisata_id="+mId);
        requestServer.getRequestMethod("POST");
        requestServer.setDialogtext("Loading");
        requestServer.execute();

        Log.i("Wisata Id", mId);

        mRecyclerViewReview = findViewById(R.id.recyclerViewReviewWisata);
        mRecyclerViewReview.setHasFixedSize(false);

        mLinearLayoutManager = new LinearLayoutManager(this);

        mRecyclerViewReview.setLayoutManager(mLinearLayoutManager);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(mDataListReviewWisata.size() > 0) {
            mDataListReviewWisata.clear();
            requestServer = new AsyncReuse(GET_WISATA_RATING, true, DetailWisata.this, getApplicationContext());
            requestServer.getResponse = DetailWisata.this;
            requestServer.getObjectQ("wisata_id="+mId);
            requestServer.getRequestMethod("POST");
            requestServer.setDialogtext("Loading");
            requestServer.execute();

            requestServer = new AsyncReuse(GET_WISATA_REVIEW, true, DetailWisata.this, getApplicationContext());
            requestServer.getResponse = DetailWisata.this;
            requestServer.getObjectQ("wisata_id="+mId);
            requestServer.getRequestMethod("POST");
            requestServer.setDialogtext("Loading");
            requestServer.execute();
        }
    }

    private void initToolbar(Bundle savedInstanceState) {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(null);

        mDetailWisataImage = findViewById(R.id.detail_wisata_image);
        mDetailWisataRatingBar = findViewById(R.id.detail_wisata_rating_bar);
        mDetailWisataTitle = findViewById(R.id.detail_wisata_title);
        mDetailWisataCreatedAt = findViewById(R.id.detail_wisata_created_at);
        mDetailWisataDescription = findViewById(R.id.detail_wisata_description);
        mFabWriteReviewDetailWisata = findViewById(R.id.fab_write_review_detail_wisata);

        mSharedPreferences = getSharedPreferences("AuthUser", MODE_PRIVATE);

        if(savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();

            if(extras == null)
            {
                this.mId = null;
                this.mImgUrl = null;
                this.mRating = null;
                this.mTitle = null;
                this.mCreatedAt = null;
                this.mDescription = null;
            } else {
                this.mId = extras.getString("id");
                this.mImgUrl = extras.getString("img");
                this.mRating = extras.getString("rating");
                this.mTitle = extras.getString("title");
                this.mCreatedAt = extras.getString("created_at");
                this.mDescription = extras.getString("description");
            }
        } else {
            this.mId = (String) savedInstanceState.getSerializable("id");
            this.mImgUrl = (String) savedInstanceState.getSerializable("img");
            this.mRating = (String) savedInstanceState.getSerializable("rating");
            this.mTitle = (String) savedInstanceState.getSerializable("title");
            this.mCreatedAt = (String) savedInstanceState.getSerializable("created_at");
            this.mDescription = (String) savedInstanceState.getSerializable("description");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        return false;
    }

    private void initNavigationMenu() {
        NavigationView nav_view = (NavigationView) findViewById(R.id.nav_view);
        final DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        nav_view.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(final MenuItem item) {
                if(item.getItemId() == R.id.nav_logout) {
                    Helper.signOut(DetailWisata.this).signOut().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Intent i = new Intent(getApplicationContext(), Login.class);
                            startActivity(i);
                        }
                    });
                } else if(item.getItemId() == R.id.nav_berita) {
                    Intent i = new Intent(getApplicationContext(), Berita.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                } else if(item.getItemId() == R.id.nav_event) {
                    Intent i = new Intent(getApplicationContext(), Event.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                } else if(item.getItemId() == R.id.nav_wisata) {
                    Intent i = new Intent(getApplicationContext(), Wisata.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                } else if(item.getItemId() == R.id.nav_informasi_public) {
                    Intent i = new Intent(getApplicationContext(), InformationPublic.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                } else if(item.getItemId() == R.id.nav_feed) {
                    Intent i = new Intent(getApplicationContext(), Feed.class);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i);
                }
                //Toast.makeText(getApplicationContext(), item.getTitle() + " Selected", Toast.LENGTH_SHORT).show();
                //actionBar.setTitle(item.getTitle());
                drawer.closeDrawers();
                return true;
            }
        });

        View viewNavDrawerHeader = nav_view.getHeaderView(0);

        Helper.setProfile(this, viewNavDrawerHeader);
    }

    private void showCustomDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_add_review);
        dialog.setCancelable(true);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        final TextView tvCurrentUserReviewDialog = dialog.findViewById(R.id.review_wisata_current_user);
        final EditText et_post = (EditText) dialog.findViewById(R.id.et_post);
        final AppCompatRatingBar rating_bar = (AppCompatRatingBar) dialog.findViewById(R.id.rating_bar);

        tvCurrentUserReviewDialog.setText(mSharedPreferences.getString("AuthUserFullname", ""));

        ((AppCompatButton) dialog.findViewById(R.id.bt_cancel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        ((AppCompatButton) dialog.findViewById(R.id.bt_submit)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String review = et_post.getText().toString().trim();
                if (review.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Please fill review text", Toast.LENGTH_SHORT).show();
                } else {
                    //items.add("(" + rating_bar.getRating() + ") " + review);
                    //adapter.notifyDataSetChanged();

                    requestServer = new AsyncReuse(POST_ADD_REVIEW_WISATA, true, DetailWisata.this, getApplicationContext());
                    requestServer.getResponse = DetailWisata.this;
                    requestServer.getObjectQ("wisata_id="+mId+"&user_id="+mSharedPreferences.getString("AuthUserId", "")+
                            "&description="+review+
                            "&rating="+rating_bar.getRating());
                    requestServer.getRequestMethod("POST");
                    requestServer.setDialogtext("Loading");
                    requestServer.execute();
                }

                dialog.dismiss();
                Toast.makeText(getApplicationContext(), "Submitted", Toast.LENGTH_SHORT).show();
            }
        });

        dialog.show();
        dialog.getWindow().setAttributes(lp);
    }

    @Override
    public void getData(String response) {

        try {
            JSONObject reviewJsonObject = new JSONObject(response.toString());

            if("submit_review".equals(reviewJsonObject.getString("type"))) {

                if("200".equals(reviewJsonObject.getString("status"))) {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                    alertDialog.setTitle("Success");
                    alertDialog.setMessage("Review berhasil di kirim.");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                            mDataListReviewWisata.clear();
                            requestServer = new AsyncReuse(GET_WISATA_REVIEW, true, DetailWisata.this, getApplicationContext());
                            requestServer.getResponse = DetailWisata.this;
                            requestServer.getObjectQ("wisata_id="+mId);
                            requestServer.getRequestMethod("POST");
                            requestServer.setDialogtext("Loading");
                            requestServer.execute();
                        }
                    });
                    alertDialog.show();

                    mDetailWisataRatingBar.setRating(Float.parseFloat(reviewJsonObject.getString("rating")));

                } else {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                    alertDialog.setTitle("Gagal");
                    alertDialog.setMessage("Silahkan coba lagi");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                }

            } else if("getWisataRating".equals(reviewJsonObject.getString("type"))) {

                if("200".equals(reviewJsonObject.getString("status"))) {

                    mDetailWisataRatingBar.setRating(Float.parseFloat(reviewJsonObject.getString("result")));
                } else {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                    alertDialog.setTitle("Gagal");
                    alertDialog.setMessage("Silahkan coba lagi");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                }
            } else if("getWisataReview".equals(reviewJsonObject.getString("type"))) {

                if("200".equals(reviewJsonObject.getString("status"))) {

                    JSONArray allReviewArray = reviewJsonObject.getJSONArray("result");

                    this.mDataListReviewWisata.removeAll(this.mDataListReviewWisata);
                    mAdapter = new CardReviewAdapter(this.mDataListReviewWisata);
                    mRecyclerViewReview.setAdapter(mAdapter);

                    for(int i=0; i < allReviewArray.length(); i++){
                        ReviewItem item = new ReviewItem();
                        JSONObject j = allReviewArray.getJSONObject(i);

                        item.setFullname(j.getString("fullname"));
                        item.setCreatedAt(j.getString("created_at"));
                        item.setDescription(j.getString("description"));
                        item.setRating(j.getString("rating"));

                        this.mDataListReviewWisata.add(item);
                    }

                } else {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                    alertDialog.setTitle("Gagal");
                    alertDialog.setMessage("Silahkan coba lagi");
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                }
            }

        } catch (JSONException e) {
            Helper.errorAlert(this);
            e.printStackTrace();
        }
    }
}
